**อัพเดตแก้ไข  7/11/2022 เพิ่ม permission 777**

- **chmod -R 777 /var/www/storage**
- **chmod -R 777 /var/www/public**

```
git clone https://gitlab.com/phatsorn.na/tqf-rmuti-kkc.git
cd tqf-rmuti-kkc

docker-compose build app
docker-compose up -d
docker-compose exec app rm -rf vendor composer.lock
docker-compose exec app composer install --ignore-platform-req=ext-zip

cp .env.example .env
docker-compose exec app php artisan key:generate

docker exec -it tqf-app bash
chown www-data:www-data /var/www
chown -R www-data:www-data /var/www/storage
chown -R www-data:www-data /var/www/bootstrap/cache
chmod -R 777 /var/www/storage  ** เพิ่ม permission file แก้ไข 7/11/22** 
chmod -R 777 /var/www/public   ** เพิ่ม permission file แก้ไข 7/11/22**

exit

docker-compose exec app php artisan config:cache
docker-compose exec app php artisan cache:clear
docker-compose exec app php artisan config:clear

-----------------------------------------------------
docker-compose exec db bash
mysql -u root -p
Enter password: 123
CREATE USER 'tqf'@'localhost' IDENTIFIED BY 'tqf1234';
GRANT ALL PRIVILEGES ON *.* TO 'tqf'@'localhost';
FLUSH PRIVILEGES;
exit

mysql -u tqf -p
CREATE DATABASE tqf;

--------------------------------------------------
> 

```
