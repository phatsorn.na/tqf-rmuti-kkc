-- MariaDB dump 10.19  Distrib 10.4.24-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: tqf
-- ------------------------------------------------------
-- Server version	10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branch` (
  `idBranch` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `factory_idfactory` int(10) unsigned NOT NULL,
  `branchName` varchar(100) NOT NULL,
  `branchcode` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idBranch`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch`
--

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
INSERT INTO `branch` VALUES (1,1,'สำนักงานคณะวิศวกรรมศาสตร์','OFFICER',NULL,'2022-10-04 22:47:20',NULL),(2,1,'วิศวกรรมคอมพิวเตอร์','ECP',NULL,'2022-10-04 20:13:26',NULL),(3,1,'วิศวกรรมไฟฟ้า','EEP',NULL,NULL,NULL),(4,1,' วิศวกรรมโยธา','ECE',NULL,NULL,NULL),(5,1,'วิศวกรรมอิเล็กทรอนิกส์และโทรคมนาคม','ENE',NULL,NULL,NULL),(6,1,'วิศวกรรมเมคคาทรอนิกส์','EMC',NULL,NULL,NULL),(7,1,'วิศวกรรมเครื่องกล','EME',NULL,NULL,NULL),(8,1,'วิศวกรรมเครื่องจักรกลเกษตร','EAE',NULL,NULL,NULL),(9,1,'วิศวกรรมอาหารและชีวภาพ','EPE',NULL,NULL,NULL),(10,1,'วิศวกรรมอุตสาหการ','EIE',NULL,NULL,NULL),(11,1,'วิศวกรรมโลหการ','EMT',NULL,NULL,NULL),(12,1,'วิชาเคมี','SCH',NULL,NULL,NULL),(13,1,'วิชาคณิตศาสตร์','',NULL,NULL,'2022-10-04 19:47:04'),(14,1,'วิชาฟิสิกส์ประยุกต์','',NULL,NULL,'2022-10-04 19:47:04'),(15,1,'วิชาสถิติประยุกต์','',NULL,NULL,'2022-10-04 19:47:04'),(16,2,'บริหาร','BIS','2022-10-04 19:47:04',NULL,NULL),(17,2,'การบัญชี','BAC','2022-10-04 20:14:17',NULL,NULL);
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `c_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `courseName` varchar(100) NOT NULL,
  `courseNumber` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'วิศวกรรมศาสตรบัณฑิต (4ปี)',4,'2022-10-03 19:55:44',NULL,NULL),(2,'บริหารธรุกิจบัณฑิต (2ปี)',2,'2022-10-03 19:56:47','2022-10-03 20:00:12',NULL),(3,'วิทยาศาสตรบัณฑิต (4 ปี)',4,NULL,NULL,NULL),(4,'วิศวกรรมศาสตรมหาบัณฑิต (2ปี ปกติ/เทียบโอน)',2,NULL,NULL,NULL),(5,'วิศวกรรมศาสตรดุษฎีบัณฑิต (3ปี ปกติ/เทียบโอน)',3,NULL,'2022-10-04 20:30:02',NULL);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `doc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `file` varchar(500) NOT NULL,
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factory`
--

DROP TABLE IF EXISTS `factory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factory` (
  `idfactory` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `factoryName` varchar(100) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idfactory`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factory`
--

LOCK TABLES `factory` WRITE;
/*!40000 ALTER TABLE `factory` DISABLE KEYS */;
INSERT INTO `factory` VALUES (1,'วิศวกรรมศาสตร์','2022-10-03 12:21:12','2022-10-03 19:29:55',NULL),(2,'บริหารธุรกิจ','2022-10-03 19:30:52',NULL,NULL),(3,'ครุศาสตร์','2022-10-03 19:31:02',NULL,NULL);
/*!40000 ALTER TABLE `factory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupstudy`
--

DROP TABLE IF EXISTS `groupstudy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupstudy` (
  `groupID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupname` varchar(500) NOT NULL,
  `course_id` int(10) unsigned DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupstudy`
--

LOCK TABLES `groupstudy` WRITE;
/*!40000 ALTER TABLE `groupstudy` DISABLE KEYS */;
INSERT INTO `groupstudy` VALUES (1,'EIE3Q',4,NULL,NULL),(2,'EIE4R1',4,NULL,NULL),(3,'EIE4R2',4,NULL,NULL),(4,'EIE4R3',4,NULL,NULL),(5,'EIE4Q',4,NULL,NULL),(6,'EMM3R',4,NULL,NULL),(7,'EMT1N',1,NULL,NULL),(8,'EMT2N',1,NULL,NULL),(9,'EMT3N',1,NULL,NULL),(10,'EMT4N',1,NULL,NULL),(11,'SCH2N',1,NULL,NULL),(12,'SCH3N',1,NULL,NULL),(13,'SCH4N',1,NULL,NULL),(14,'MIP1N',1,NULL,NULL),(15,'EIE3R',4,NULL,NULL),(16,'EIE2Q',4,NULL,NULL),(17,'EIE2R',4,NULL,NULL),(18,'EIE4N',1,NULL,NULL),(19,'EIE3N PE',1,NULL,NULL),(20,'EIE3N IE',1,NULL,NULL),(21,'EIE2N2',1,NULL,NULL),(22,'EIE2N1',1,NULL,NULL),(23,'EIE1N2',1,NULL,NULL),(24,'EIE1N1',1,NULL,NULL),(25,'EPE4N',1,NULL,NULL),(26,'EPE3N',1,NULL,NULL),(27,'EPE2N',1,NULL,NULL),(28,'EPE1N',1,NULL,NULL),(29,'EMS4R',4,NULL,NULL),(30,'EMS3R',4,NULL,NULL),(31,'EAE4R',4,NULL,NULL),(32,'EAE3R',4,NULL,NULL),(33,'EAE2R',4,NULL,NULL),(34,'EAE4N',1,NULL,NULL),(35,'EAE3N',1,NULL,NULL),(36,'EAE2N',1,NULL,NULL),(37,'EAE1N',1,NULL,NULL),(38,'EME4Q',4,NULL,NULL),(39,'EME4R',4,NULL,NULL),(40,'EME3Q',4,NULL,NULL),(41,'EME3R',4,NULL,NULL),(42,'EME2Q',4,NULL,NULL),(43,'EME2R',4,NULL,NULL),(44,'EME4NR',1,NULL,NULL),(45,'EME3NR',1,NULL,NULL),(46,'EME2NR',1,NULL,NULL),(47,'EME1NR',1,NULL,NULL),(48,'EME4NM',1,NULL,NULL),(49,'EME3NM',1,NULL,NULL),(50,'EME2NM',1,NULL,NULL),(51,'EME1NM',1,NULL,NULL),(52,'ECP4R',4,NULL,NULL),(53,'ECP3R',4,NULL,NULL),(54,'ECP2R',4,NULL,NULL),(55,'ECP4N',1,NULL,NULL),(56,'ECP3N',1,NULL,NULL),(57,'ECP2N',1,NULL,NULL),(58,'ECP1N',1,NULL,NULL),(59,'ENS4R',4,NULL,NULL),(60,'ENS3R',4,NULL,NULL),(61,'ENE4R2',4,NULL,NULL),(62,'ENE4R1',4,NULL,NULL),(63,'ENT3R',4,NULL,NULL),(64,'ENT2R',4,NULL,NULL),(65,'ENE4N',1,NULL,NULL),(66,'ENE3N',1,NULL,NULL),(67,'ENT2N',1,NULL,NULL),(68,'ENT1N',1,NULL,NULL),(69,'EMC4N',1,NULL,NULL),(70,'EMC3N',1,NULL,NULL),(71,'EMC2N',1,NULL,NULL),(72,'EMC1N',1,NULL,NULL),(73,'EEP4R2',4,NULL,NULL),(74,'EEP4R1',4,NULL,NULL),(75,'EEP3R2',4,NULL,NULL),(76,'EEP3R1',4,NULL,NULL),(77,'EEP2R2',4,NULL,NULL),(78,'EEP2R1',4,NULL,NULL),(79,'EEP4NR',1,NULL,NULL),(80,'EEP3NR',1,NULL,NULL),(81,'EEP2NR',1,NULL,NULL),(82,'EEP1NR',1,NULL,NULL),(83,'EEP4NP',1,NULL,NULL),(84,'EEP3NP',1,NULL,NULL),(85,'EEP2NP',1,NULL,NULL),(86,'EEP1NP',1,NULL,NULL),(87,'ECE4Q',4,NULL,NULL),(88,'ECE4R',4,NULL,NULL),(89,'ECE3Q',4,NULL,NULL),(90,'ECE3R',4,NULL,NULL),(91,'ECE2R2',4,NULL,NULL),(92,'ECE2R1',4,NULL,NULL),(93,'ECE4NR',1,NULL,NULL),(94,'ECE3NR',1,NULL,NULL),(95,'ECE2NR',1,NULL,NULL),(96,'ECE4NC',1,NULL,NULL),(97,'ECE3NC',1,NULL,NULL),(98,'ECE2NC',1,NULL,NULL),(99,'ECE1NC',1,NULL,NULL),(100,'MEME2Q',2,NULL,NULL),(101,'MEME2N',2,NULL,NULL),(102,'MEME1Q',2,NULL,NULL),(103,'MEME1N',2,NULL,NULL),(104,'MEEP2Q',2,NULL,NULL),(105,'MEEP1Q',2,NULL,NULL),(106,'MEEP1N',2,NULL,NULL),(107,'DENE3R',3,NULL,NULL),(108,'DENE2Q',3,NULL,NULL),(109,'DENE1Q',3,NULL,NULL),(110,'MECE2Q',2,NULL,NULL),(111,'MECE2N',2,NULL,NULL),(112,'MECE1Q',2,NULL,NULL),(113,'MECE1N',2,NULL,NULL),(114,'EFE3N',1,NULL,NULL),(115,'EFE4N',1,NULL,NULL),(116,'EIE2N2',1,NULL,NULL),(117,'EIE3NP',1,NULL,NULL),(118,'EIE3NI',1,NULL,NULL),(119,'SCH1N',1,NULL,NULL),(120,'BAC',7,'2022-10-04 19:53:34','2022-10-04 23:52:50'),(121,'BAC1N',7,'2022-10-04 20:21:05',NULL);
/*!40000 ALTER TABLE `groupstudy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `level`
--

DROP TABLE IF EXISTS `level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `level` (
  `levelID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `levelName` varchar(50) NOT NULL,
  PRIMARY KEY (`levelID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `level`
--

LOCK TABLES `level` WRITE;
/*!40000 ALTER TABLE `level` DISABLE KEYS */;
INSERT INTO `level` VALUES (1,'เจ้าหน้าที่'),(2,'อาจารย์');
/*!40000 ALTER TABLE `level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2020_08_03_172806_create_factory_table',1),(2,'2020_08_03_185216_create_users_table',1),(3,'2020_08_03_202548_create_managetqf_table',1),(4,'2020_08_13_144825_create_tqf3_table',1),(5,'2020_08_13_163536_create_tqf5_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `idnews` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateupdate` datetime NOT NULL,
  `pic` blob DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`idnews`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'ประชาสัมพันธ์','<p style=\"text-align: center; \"><img style=\"width: 744px;\" src=\"https://www.kkc.rmuti.ac.th/2017/wp-content/uploads/2022/08/messageImage_1661590444582-1024x576.jpg\"></p>','2022-10-05 11:34:09','','2022-09-04 00:00:00',1),(12,'🔹🔶มหาวิทยาลัยเทคโนโลยีราชมงคลอีสาน วิทยาเขตขอนแก่น  ขอแสดงความยินดีกับ','<p><img src=\"file_photo/305381452_5489153681151275_7096042606181482153_n.jpg\" style=\"width: 50%;\"><img src=\"file_photo/305487514_5489153744484602_2463934985519586269_n.jpg\" style=\"width: 50%;\"></p><p>ข่าว : ทัศนีย์ เรืองวงศ์วิทยา</p><p>กราฟิก : พิมพ์ฤทัย ชัยวิเศษ</p>','2022-09-09 17:59:28','','2022-09-06 09:27:18',1);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `idsubject` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subjectCode` text DEFAULT NULL,
  `ENsubject` text DEFAULT NULL,
  `THsubject` text DEFAULT NULL,
  `cradit` text DEFAULT NULL,
  `subNumber` varchar(10) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idsubject`)
) ENGINE=InnoDB AUTO_INCREMENT=384 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (1,'04-061-209','Computer Architecture and Organization 1','สถาปัตยกรรมคอมพิวเตอร์และระบบ 1','2(2-0-4)','1.2','2022-10-04 00:48:32',NULL,NULL),(2,'04-061-309','Operating System','ระบบปฏิบัติการ','3 (3-0-6)','1.2','2022-10-04 00:48:32',NULL,NULL),(3,'31-607-012-101','Research Methodology in Civil Engineering','ระเบียบวิจัยทางวิศวกรรมโยธา','3(3-0-6)','1','2022-10-04 00:48:32',NULL,NULL),(4,'31-607-012-102','Civil Engineering Seminar 1','สัมมนาทางวิศวกรรมโยธา 1','1(1-2-3)','1','2022-10-04 00:48:32',NULL,NULL),(5,'31-607-012-104','Advanced Mechanics of Materials','กลศาสตร์วัสดุขั้นสูง','3(3-0-6)','1','2022-10-04 00:48:32',NULL,NULL),(6,'31-607-011-022','Microstructure and Durability of Concrete','โครงสร้างจุลภาคและความทนทานของคอนกรีต','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(7,'31-607-011-024','Advanced Concrete Technology','เทคโนโลยีคอนกรีตขั้นสูง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(8,'31-607-011-026','Experimental Method in Civil Engineering','วิธีการทดลองในงานวิศวกรรมโยธา','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(9,'31-607-011-031','Advanced Soil Mechanics','ปฐพีกลศาสตร์ขั้นสูง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(10,'31-607-011-035','Geomechanics','ธรณีกลศาสตร์','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(11,'31-607-011-052','Advanced Reinforced Concrete Structures','โครงสร้างคอนกรีตเสริมเหล็กขั้นสูง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(12,'31-607-011-072','Computational Hydraulics','ชลศาสตร์เชิงคำนวณ','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(13,'31-607-011-076','Geographic Information System for water resource engineering','ระบบสารสนเทศภูมิศาสตร์สำหรับวิศวกรรมทรัพยากรแหล่งนํ้า','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(14,'31-607-011-051','Finite Element Method','วิธีไฟไนท์อิลลิเมนต์','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(15,'31-607-011-091','Transportation System Analysis','การวิเคราะห์ระบบขนส่ง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(16,'31-607-011-093','Urban Transportaion Planning','การวางแผนการขนส่งในเมือง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(17,'31-607-014-002','Thesis','วิทยานิพนธ์','0(0-0-0)','3','2022-10-04 00:48:32',NULL,NULL),(18,'31-607-011-075','Control of Flood and drought','การควบคุมนํ้าท่วมและภาวะภัยแล้ง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(19,'31-607-011-037','Deep Excavation and Tunneling','งานดินขุดระดับลึกและการเจาะอุโมงค์','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(20,'31-607-011-039','Soil Investigation and Intepretation for Geotechnical Engineering','การสำรวจชั้นดินและการแปลผลเพื่องานด้านวิศวกรรมธรณีเทคนิค','3(2-2-5)','2','2022-10-04 00:48:32',NULL,NULL),(21,'31-607-011-092','Traffic Engineering','วิศวกรรมจราจร','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(22,'31-607-011-094','Data Analytics for Transportation Engineering','วิทยาการวิเคราะห์ข้อมูลสำหรับวิศวกรรมการขนส่ง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(23,'31-807-041-103','Doctoral Seminar 3','สัมมนาปริญญาเอก 3','1(1-0-2)','1','2022-10-04 00:48:32',NULL,NULL),(24,'31-807-041-106','Advanced Research Methodology 1','ระเบียบวิธีวิจัยขั้นสูง 1','3(3-0-6)','1','2022-10-04 00:48:32',NULL,NULL),(25,'31-807-041-107','Advanced Electrical Engineering Mathematics','คณิตศาสตร์วิศวกรรมไฟฟ้าขั้นสูง','3(3-0-6)','1','2022-10-04 00:48:32',NULL,NULL),(26,'31-807-042-209','Advanced Filter Design','การออกแบบวงจรกรองขั้นสูง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(27,'31-807-042-208','Advanced Circuit Design','การออกแบบวงจรขั้นสูง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(28,'31-607-041-101','Advanced Electrical Engineering Mathematics','คณิตศาสตร์วิศวกรรมไฟฟ้าขั้นสูง','3(3-0-6)','1','2022-10-04 00:48:32',NULL,NULL),(29,'31-607-041-104','Electrical Engineering Seminar 1','สัมมนาทางวิศวกรรมไฟฟ้า 1','1(1-0-2)','1','2022-10-04 00:48:32',NULL,NULL),(30,'31-607-042-022','Advanced Digital Signal Processing','การประมวลผลสัญญาณดิจิทัลขั้นสูง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(31,'31-607-042-023','Muti-Carrier Digital Communications','การสื่อสารแบบดิจิทัลโดยใช้หลายคลื่นพาห์','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(32,'31-407-042-004','Electrical Machines and Drives','เครื่องจักรกลไฟฟ้าและการขับเคลื่อน','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(33,'31-607-042-009','Selected Topics in Electrical Engineering for Railways System','หัวข้อเลือกสรรทางวิศวกรรมไฟฟ้าสำหรับระบบราง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(34,'31-607-042-006','Artificial Intelligence for Electrical Engineering','ปัญญาประดิษฐ์สำหรับวิศวกรรมไฟฟ้า','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(35,'31-607-042-010','Selected Topics in Electrical Engineering ','หัวข้อพิเศษทางด้านวิศวกรรมไฟฟ้า','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(36,'31-607-071-101','Advanced Mathematics for Mechanical Engineers','คณิตศาสตร์ขั้นสูงสำหรับวิศวกรเครื่องกล','3(3-0-6)','1','2022-10-04 00:48:32',NULL,NULL),(37,'31-607-071-102','Research Methodology for Mechanical Engineers','ระเบียบวิธีวิจัยสำหรับวิศวกรเครื่องกล','3(3-0-6)','1','2022-10-04 00:48:32',NULL,NULL),(38,'31-607-070-102','Artificial Intelligence in Mechanical Engineering','ปัญญาประดิษฐ์ทางวิศวกรรมเครื่องกล','3(3-0-6)','1','2022-10-04 00:48:32',NULL,NULL),(39,'31-607-072-102','Renewable Energy Resources','ทรัพยากรพลังงานทดแทน','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(40,'31-607-073-102','Thesis Plan A Model A2','วิทยานิพนธ์ แผน ก แบบ ก 2','0(0-0-0)','3','2022-10-04 00:48:32',NULL,NULL),(41,'31-607-072-103','Solar Energy','พลังงานสุริยะ','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(42,'31-607-072-110','Intelligent Systems and Machine Leaming','ระบบอัจฉริยะและการเรียนรู้ของเครื่อง','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(43,'31-607-072-114','Selected Topics in Mechanical Engineering','หัวข้อเลือกสรรทางวิศวกรรมเครื่องกล','3(3-0-6)','2','2022-10-04 00:48:32',NULL,NULL),(44,'00-000-023-001','Sport and Recreation for Health','กีฬาและนันทนาการเพื่อสุขภาพ','3(2-2-5)','1.2','2022-10-04 00:48:32',NULL,NULL),(45,'00-000-031-101','English for Study Skills Development','ภาษาอังกฤษเพื่อพัฒนาทักษะการเรียน','3(3-0-6)','1.3','2022-10-04 00:48:32',NULL,NULL),(46,'02-005-011-109','Calculus 1 for Engineers','แคลคูลัส 1 สำหรับวิศวกร','3(3-0-6)','2.1.1','2022-10-04 00:48:32',NULL,NULL),(47,'02-005-020-105','Fundamentals of Chemistry','เคมีพื้นฐาน','3(3-0-6)','2.1.1','2022-10-04 00:48:32',NULL,NULL),(48,'02-005-020-106','Fundamentals of Chemistry Laboratory','ปฏิบัติการเคมีพื้นฐาน','1(0-3-1)','2.1.1','2022-10-04 00:48:32',NULL,NULL),(49,'02-005-030-101','Physics 1','ฟิสิกส์ 1','3(3-0-6)','2.1.1','2022-10-04 00:48:32',NULL,NULL),(50,'02-005-030-102','Physics Laboratory 1','ปฏิบัติการฟิสิกส์ 1','1(0-3-1)','2.1.1','2022-10-04 00:48:32',NULL,NULL),(51,'31-407-120-101','Engineering Materials','วัสดุวิศวกรรม','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(52,'31-407-012-102','Civil Engineering Workshop 1','การปฏิบัติงานวิศวกรรมโยธา 1','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(53,'00-000-012-001','Life and Social Quality Development','การพัฒนาคุณภาพชีวิตและสังคม','3(3-0-6)','1.1','2022-10-04 00:48:32',NULL,NULL),(54,'00-000-022-001','Human Value : Arts and Sciences in Daily Living','คุณค่าของมนุษย์ : ศิลป์และศาสตร์ในการดำเนินชีวิต','3(3-0-6)','1.2','2022-10-04 00:48:32',NULL,NULL),(55,'00-000-031-203','English Reading for Academic Purposes','การอ่านภาษาอังกฤษเชิงวิชาการ','3(3-0-6)','1.3','2022-10-04 00:48:32',NULL,NULL),(56,'02-005-011-211','Calculus 3 for Engineers','แคลคูลัส 3 สำหรับวิศวกร','3(3-0-6)','2.1.1','2022-10-04 00:48:32',NULL,NULL),(57,'31-407-010-201','Strength of Materials','ความแข็งแรงของวัสดุ','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(58,'31-407-010-204','Surveying','การสำรวจ','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(59,'31-407-010-205','Surveying Practice','ปฏิบัติการสำรวจ','1(0-3-1)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(60,'31-407-013-210','Civil Engineering Drawing','การเขียนแบบวิศวกรรมโยธา','1(0-3-1)','2.2.2','2022-10-04 00:48:32',NULL,NULL),(61,'31-407-010-203','Hydraulic Laboratory','ปฏิบัติการชลศาสตร์','1(0-3-1)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(62,'31-407-010-308','Field Survey','การสำรวจภาคสนาม','1(0-3-1)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(63,'31-407-010-320','Applied Mathematics for Civil Engineers','คณิตศาสตร์ประยุกต์สำหรับวิศวกรโยธา','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(64,'31-407-011-301','Structural Analysis','การวิเคราะห์โครงสร้าง','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(65,'31-407-011-302','Hydrology','อุทกวิทยา','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(66,'31-407-011-303','Soil Mechanics','ปฐพีกลศาสตร์','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(67,'31-407-011-304','Soil Mechanics Laboratory','ปฏิบัติการปฐพีกลศาสตร์','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(68,'31-407-011-305','Reinforced Concrete Design','การออกแบบคอนกรีตเสริมเหล็ก','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(69,'31-407-011-306','Reinforced Concrete Design Practice','ปฏิบัติการออกแบบคอนกรีตเสริมเหล็ก','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(70,'31-407-012-411','Cooperative Education for Civil Engineering','สหกิจศึกษา สำหรับวิศวกรรมโยธา','6(0-40-0)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(71,'04-012-410','Strength of materials 2','ความแข็งแรงของวัสดุ 2','3(3-0-6)','2.3','2022-10-04 00:48:32',NULL,NULL),(72,'04-012-415','Transportation Engineering','วิศวกรรมขนส่ง','3(3-0-6)','2.3','2022-10-04 00:48:32',NULL,NULL),(73,'04-011-420','Civil Engineering Project','โครงงานวิศวกรรมโยธา','3(1-6-4)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(74,'02-011-318','Ordinary Differential Equation','สมการเชิงอนุพันธ์สามัญ','3(3-0-6)','2.1','2022-10-04 00:48:32',NULL,NULL),(75,'31-407-015-251','Raiway Civil Engineering Drawing','การเขียนแบบวิศวกรรมโยธาระบบราง','1(0-3-1)','2.2.2','2022-10-04 00:48:32',NULL,NULL),(76,'31-407-100-101','Computer Programming','การโปรแกรมคอมพิวเตอร์','3(2-3-5)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(77,'31-407-050-102','Engineering Drawing','การเขียนแบบวิศวกรรม','3(2-3-5)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(78,'00-000-042-001','Mathematics and Statistics for Daily Life','คณิตศาสตร์และสถิติที่ใช้ในชีวิตประจำวัน','3(3-0-6)','1.4','2022-10-04 00:48:32',NULL,NULL),(79,'31-407-010-202','Hydraulics','ชลศาสตร์','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(80,'31-407-011-206','Theory of Structures','ทฤษฎีโครงสร้าง','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(81,'31-407-011-207','Civil Engineering Materials and Testing','วัสดุวิศวกรรมโยธาและการทดสอบ','3(2-3-5)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(82,'31-407-013-307','Route Suveying','การสำรวจเส้นทาง','3(2-3-5)','2.2.2','2022-10-04 00:48:32',NULL,NULL),(83,'31-407-011-208','Materials Testing Laboratory','ปฏิบัติการทดสอบวัสดุ','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(84,'31-407-010-101','Statics','สถิตยศาสตร์','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(85,'31-407-012-412','Foundation Engineering','หัวข้อพิเศษในงานวิศวกรรมโยธา','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(86,'31-407-012-313','Highway Engineering','วิศวกรรมการทาง','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(87,'31-407-012-314','Highway Materials Testing','การทดสอบวัสดุการทาง','1(0-3-1)','2.2','2022-10-04 00:48:32',NULL,NULL),(88,'31-407-011-401','Timber and Steel Design','การออกแบบโครงสร้างไม้และเหล็ก','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(89,'31-407-011-402','Timber and Steel Design Practice','ปฏิบัติการออกแบบโครงสร้างไม้และเหล็ก','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(90,'31-407-011-312','Hydraulic Engineering','วิศวกรรมชลศาสตร์','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(91,'31-407-012-318','Civil Engineering Project Seminar','สัมมนาโครงงานวิศวกรรมโยธา','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(92,'31-407-013-311','Construction Cost Estimation and Analysis','การประมาณและวิเคราะห์ราคางานก่อสร้าง','3(2-3-5)','2.2.2','2022-10-04 00:48:32',NULL,NULL),(93,'31-407-013-406','Prestressed Concrete Design','การออกแบบคอนกรีตอัดแรง','3(3-0-6)','3','2022-10-04 00:48:32',NULL,NULL),(94,'31-407-050-101','Basic Engineering Training','การฝึกพื้นฐานทางวิศวกรรม','3(1-6-4)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(95,'00-000-021-001','Information Literacy Skills','ทักษะการรู้สารสนเทศ','3(3-0-6)','1.2','2022-10-04 00:48:32',NULL,NULL),(96,'00-000-041-003','Science for Health','วิทยาศาสตร์เพื่อสุขภาพ','3(3-0-6)','1.4','2022-10-04 00:48:32',NULL,NULL),(97,'31-407-030-202','Electric Circuits 1','วงจรไฟฟ้า 1','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(98,'31-407-030-204','Electromagnetic Fields','สนามแม่เหล็กไฟฟ้า','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(99,'31-407-030-207','Engineering Electronics','อิเล็กทรอนิกส์วิศวกรรม','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(100,'31-407-030-208','Engineering Electronics Laboratory','ปฏิบัติการอิเล็กทรอนิกส์วิศวกรรม','1(0-3-1)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(101,'31-407-070-204','Engineering Mechanics','กลศาสตร์วิศวกรรม','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(102,'31-407-030-305','Control System','ระบบควบคุม','3(3-0-6)','2.1.2','2022-10-04 00:48:32',NULL,NULL),(103,'31-407-031-309','Electrical Machines 2','เครื่องจักรกลไฟฟ้า 2','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(104,'31-407-031-310','Electrical Machines  Laboratory','ปฏิบัติการเครื่องจักรกลไฟฟ้า','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(105,'31-407-032-302','Electrical Power Transmission and Distribution','การส่งจ่ายและจำหน่ายกำลังไฟฟ้า','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(106,'31-407-032-303','Power Plant and Substation','โรงต้นกำลังและสถานีไฟฟ้าย่อย','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(107,'31-407-034-304','Programmable Logic Controls System','ระบบควบคุมแบบลำดับที่โปรแกรมได้','3(2-3-5)','2.2.2','2022-10-04 00:48:32',NULL,NULL),(108,'31-407-034-303','Numerical Methods for Engineering','ระเบียบวิธีเชิงเลขสำหรับงานวิศวกรรม','3(3-0-6)','3','2022-10-04 00:48:32',NULL,NULL),(109,'31-407-032-402','Cooperative Education for Electrical Engineering','สหกิจศึกษาสำหรับวิศวกรรมไฟฟ้า','6(0-40-0)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(110,'31-407-033-302','Introduction to Railway System Engineering','วิศวกรรมระบบรางเบื้องต้น','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(111,'31-407-033-303','Railway Electrification system','ระบบไฟฟ้าสำหรับระบบราง','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(112,'31-407-035-303','Basic Railway Maintenance','การซ่อมบำรุงระบบรางเบื้องต้น','3(3-0-6)','2.2.2','2022-10-04 00:48:32',NULL,NULL),(113,'31-407-035-405','Railway Signaling System','ระบบอาณัติสัญญาณสำหรับระบบราง','3(3-0-6)','3','2022-10-04 00:48:32',NULL,NULL),(114,'31-407-031-201','Electric Circuits 2','วงจรไฟฟ้า 2','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(115,'31-407-031-202','Electric Circuits Laboratory','ปฏิบัติการวงจรไฟฟ้า','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(116,'31-407-031-204','Electrical Machines 1','เครื่องจักรกลไฟฟ้า 1','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(117,'31-407-031-206','Electrical Instruments and Measurements','เครื่องมือวัดและการวัดทางไฟฟ้า','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(118,'31-407-031-208','Digital Circuit Laboratory','ปฏิบัติการวงจรดิจิตอล','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(119,'31-407-031-320','Power Electronics Laboratory','ปฏิบัติการอิเล็กทรอนิกส์กำลัง','1(0-3-1)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(120,'31-407-031-307','Electrical System Design','การออกแบบระบบไฟฟ้า','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(121,'31-407-031-308','Electrical Power System Analysis','การวิเคราะห์ระบบไฟฟ้ากำลัง','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(122,'31-407-032-420','Electric Drives','การขับเคลื่อนด้วยไฟฟ้า','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(123,'31-407-032-401','Electrical Engineering Project','โครงงานวิศวกรรมไฟฟ้า','3(1-6-4)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(124,'31-407-032-417','High Voltage Engineering','วิศวกรรมไฟฟ้าแรงสูง','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(125,'31-407-032-403','Power System Protection','การป้องกันระบบไฟฟ้ากำลัง','3(3-0-6)','2.2.1','2022-10-04 00:48:32',NULL,NULL),(126,'00-000-032-101','Thai for Communication','ภาษาไทยเพื่อการสื่อสาร','3(3-0-6)','1.3','2022-10-04 00:48:32',NULL,NULL),(127,'31-407-130-103','Manufacturing Processes','กระบวนการผลิต','3(3-0-6)','2.1','2022-10-04 00:48:32',NULL,NULL),(128,'31-407-131-202','Production Planning and Control','การวางแผนและควบคุมการผลิต','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(129,'31-407-132-201','Numerical Method for Mechatronics Engineering','วิธีเชิงตัวเลขสำหรับวิศวกรรมเมคคาทรอนิกส์','3(3-0-6)','2.3','2022-10-04 00:48:32',NULL,NULL),(130,'31-407-130-201','Electric Circuit 1','วงจรไฟฟ้า 1','3(3-0-6)','2.1','2022-10-04 00:48:32',NULL,NULL),(131,'31-407-130-202','Electric Circuit Laboratory 1','ปฏิบัติการวงจรไฟฟ้า 1','1(0-3-1)','2.1','2022-10-04 00:48:32',NULL,NULL),(132,'31-407-130-205','Thermo-Fluid Engineering','วิศวกรรมความร้อนและของไหล','3(3-0-6)','2.1','2022-10-04 00:48:32',NULL,NULL),(133,'00-000-031-102','English for Communication','ภาษาอังกฤษเพื่อการสื่อสาร','3(3-0-6)','1.3','2022-10-04 00:48:32',NULL,NULL),(134,'31-407-131-302','Programmable Logic Controllers','โปรแกรมเมเบิลลอจิกคอนโทรลเลอร์','3(2-3-5)','2.2','2022-10-04 00:48:32',NULL,NULL),(135,'31-407-131-303','Electric Motor Drive','การขับเคลื่อนมอเตอร์ไฟฟ้า','3(2-3-5)','2.2','2022-10-04 00:48:32',NULL,NULL),(136,'31-407-131-304','Robotics and Applications','หุ่นยนต์และการประยุกต์ใช้งาน','2(1-3-3)','2.2','2022-10-04 00:48:32',NULL,NULL),(137,'31-407-131-305','Microcontrollers and Applications','ไมโครคอนโทรเลอร์และการประยุกต์ใช้งาน','2(1-3-3)','2.2','2022-10-04 00:48:32',NULL,NULL),(138,'31-407-132-203','Pneumatics and Hydraulics Control','การควบคุมนิวแมติกส์และไฮดรอลิกส์','3(2-3-5)','2.3','2022-10-04 00:48:32',NULL,NULL),(139,'00-000-031-205','English Writing for Daily Life','การเขียนภาษาอังกฤษในชีวิตประจำวัน','3(3-0-6)','1.3','2022-10-04 00:48:32',NULL,NULL),(140,'31-407-131-401','Industrial Plant Design','การออกแบบโรงงานอุตสาหกรรม','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(141,'31-407-131-402','Mechatronics Engineering Project','โครงงานวิศวกรรมเมคคาทรอนิกส์','3(1-6-4)','2.2','2022-10-04 00:48:32',NULL,NULL),(142,'31-407-132-318','Robotics and Machine Vision','หุ่นยนต์และการรับภาพของเครื่อง','3(2-3-5)','3','2022-10-04 00:48:32',NULL,NULL),(143,'31-407-132-415','Select Topics in Mechatronics Engineering','เรื่องเฉพาะวิศวกรรมเมคคาทรอนิกส์','3(3-0-6)','3','2022-10-04 00:48:32',NULL,NULL),(144,'00-000-011-002','Social Dynamics and Happy Living','พลวัตทางสังคมกับการดำรงชีวิตอย่างมีความสุข','3(3-0-6)','1.1','2022-10-04 00:48:32','2022-10-04 20:40:28',NULL),(145,'31-407-450-201','Electric Circuits','วงจรไฟฟ้า','3(3-0-6)','2.1','2022-10-04 00:48:32',NULL,NULL),(146,'31-407-450-202','Electric Circuits Laboratory','ปฏิบัติการวงจรไฟฟ้า','1(0-3-1)','2.2','2022-10-04 00:48:32',NULL,NULL),(147,'31-407-450-203','Engineering Electronics','อิเล็กทรอนิกส์วิศวกรรม','3(3-0-6)','2.1','2022-10-04 00:48:32',NULL,NULL),(148,'31-407-450-204','Engineering Electronics Laboratory','ปฏิบัติการอิเล็กทรอนิกส์วิศวกรรม','1(0-3-1)','2.2','2022-10-04 00:48:32',NULL,NULL),(149,'31-407-451-202','Singnal and Systems ','สัญญาณและระบบ','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(150,'03-407-041-204','Digital Communication','การสื่อสารดิจิตอล','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(151,'03-407-041-209','Communication Networks and Transmission Lines','สายส่งและโครงข่ายการสื่อสาร','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(152,'03-407-041-210','Data Communications and Network','การสื่อสารข้อมูลและเครือข่าย','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(153,'03-407-041-314','Principle Electrical Measurements and Instrumentation','หลักการวัดและเครื่องมือวัดทางไฟฟ้า','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(154,'03-407-041-315','Feedback Control Systems','ระบบควบคุมป้อนกลับ','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(155,'03-407-042-414','Microprocessor','ไมโครโพรเซสเซอร์','3(3-0-6)','2.3','2022-10-04 00:48:32',NULL,NULL),(156,'03-407-042-415','Microprocessor Laboratory','ปฏิบัติการไมโครโพรเซสเซอร์','1(0-3-1)','2.3','2022-10-04 00:48:32',NULL,NULL),(157,'03-407-042-302','Electromagnetic Waves','คลื่นแม่เหล็กไฟฟ้า','3(3-0-6)','3','2022-10-04 00:48:32',NULL,NULL),(158,'03-407-041-205','Optical Communication','การสื่อสารทางแสง','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(159,'03-407-041-206','Optical Communication Laboratory','ปฏิบัติการสื่อสารทางแสง','1(0-3-1)','2.2','2022-10-04 00:48:32',NULL,NULL),(160,'03-407-041-211','Microwave Engineering','วิศวกรรมไมโครเวฟ','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(161,'03-407-041-308','Pre-Cooperative Education for Engineering','การเตรียมความพร้อมก่อนสหกิจศึกษาสำหรับวิศวกรรม','2(2-0-4)','2.2','2022-10-04 00:48:32',NULL,NULL),(162,'03-407-041-312','Antenna Engineering','วิศวกรรมสายอากาศ','3(3-0-6)','2.2','2022-10-04 00:48:32',NULL,NULL),(163,'03-407-041-313','Antenna Engineering Laboratory','ปฏิบัติการวิศวกรรมสายอากาศ','1(0-3-1)','2.2','2022-10-04 00:48:32',NULL,NULL),(164,'03-407-041-417','Electronics and Telecommunication Project','โครงงานวิศวกรรมอิเล็กทรอนิกส์และโทรคมนาคม','3(1-6-4)','2.2','2022-10-04 00:48:32',NULL,NULL),(165,'03-407-042-417','Microwave Engineering Laboratory','ปฏิบัติการวิศวกรรมไมโครเวฟ','1(0-3-1)','2.3','2022-10-04 00:48:32',NULL,NULL),(166,'03-407-042-418','Radio Wave Propagation','การแพร่กระจายคลื่นวิทยุ','3(3-0-6)','3','2022-10-04 00:48:32',NULL,NULL),(167,'02-030-104','Physics Laboratory 1','ปฏิบัติการฟิสิกส์ 2','1(0-3-1)','2.1','2022-10-04 00:48:32',NULL,NULL),(168,'31-407-450-301','Control Systems','ระบบควบคุม','3(3-0-6)','2.1','2022-10-04 00:48:32',NULL,NULL),(169,'31-407-450-206','Digital Circuits and Logic Design','วงจรดิจิทัลและการออกแบบลอจิก','3(3-0-6)','2.3','2022-10-04 00:48:32',NULL,NULL),(170,'31-407-450-207','Digital Circuits Laboratory','ปฏิบัติการวงจรดิจิทัลและการออกแบบลอจิก','1(0-3-1)','2.3','2022-10-04 00:48:32',NULL,NULL),(171,'31-407-450-205','Electromagnetic Fields','สนามแม่เหล็กไฟฟ้า','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(172,'03-407-041-208','Principle of Communication Systems','หลักการของระบบสื่อสาร','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(173,'03-407-042-201','Communication Systems Laboratory','ปฏิบัติการระบบสื่อสาร','1(0-3-1)','2.3','2022-10-04 00:48:33',NULL,NULL),(174,'03-407-041-418','Cooperative Education for Electronics and Telecommunication Engineering','สหกิจศึกษาสำหรับวิศวกรรมอิเล็กทรอนิกส์และโทรคมนาคม','6(0-40-0)','2.3','2022-10-04 00:48:33',NULL,NULL),(175,'03-407-042-420','Applied Engineering Mathematics','คณิตศาสตร์วิศวกรรมประยุกต์','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(176,'31-407-360-301','Applied Computation for Smart Electronics Engineering','การคำนวณเชิงประยุกต์สำหรับวิศวกรรมอิเล็กทรอนิกส์อัจฉริยะ','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(177,'31-407-360-302','Applied Science for Smart Electronics Engineering','วิทยาศาสตร์ประยุกต์สำหรับวิศวกรรมอิเล็กทรอนิกส์ อัจฉริยะ','3(2-3-5)','2.1','2022-10-04 00:48:33',NULL,NULL),(178,'31-407-361-301','Electronic Circuits Design and PCB Fabrication','การออกแบบวงจรอิเล็กทรอนิกส์และการผลิตแผ่นวงจรพิมพ์','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(179,'31-407-361-302','Electronic Circuits Design and PCB Fabrication Laboratory','ปฏิบัติการออกแบบวงจรอิเล็กทรอนิกส์และการผลิตแผ่นวงจรพิมพ์','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(180,'31-407-361-303','Power Electronics and Motor Drive Circuit for Electrical Vehicle','อิเล็กทรอนิกส์กำลังและวงจรขับมอเตอร์สำหรับยานยนต์ไฟฟ้า','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(181,'31-407-362-301','Mixed Signal Integrated Circuit Design using CMOS Technology','การออกแบบวงจรรวมแบบสัญญาณผสมโดยใช้เทคโนโลยีซีมอส','3(3-0-6)','3','2022-10-04 00:48:33',NULL,NULL),(182,'31-407-361-401','Practice on Smart Electronics Engineering 1','การฝึกปฏิบัติวิศวกรรมอิเล็กทรอนิกส์อัจฉริยะ 1','2(0-6-2)','2.2','2022-10-04 00:48:33',NULL,NULL),(183,'31-407-361-402','Practice on Smart Electronics Engineering 2','การฝึกปฏิบัติวิศวกรรมอิเล็กทรอนิกส์อัจฉริยะ 2','2(0-6-2)','2.2','2022-10-04 00:48:33',NULL,NULL),(184,'31-407-361-403','Practice on Smart Electronics Engineering 3','การฝึกปฏิบัติวิศวกรรมอิเล็กทรอนิกส์อัจฉริยะ 3','2(0-6-2)','2.2','2022-10-04 00:48:33',NULL,NULL),(185,'31-407-361-404','Practice on Smart Electronics Engineering 4','การฝึกปฏิบัติวิศวกรรมอิเล็กทรอนิกส์อัจฉริยะ 4','2(0-6-2)','2.2','2022-10-04 00:48:33',NULL,NULL),(186,'31-407-361-409','Smart Electronics Engineering Project 1','โครงงานปฏิบัติวิศวกรรมอิเล็กทรอนิกส์อัจฉริยะ 1','3(1-6-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(187,'31-407-100-102','Fundamental Computer Engineering','วิศวกรรมคอมพิวเตอร์พื้นฐาน','3(2-3-5)','2.1','2022-10-04 00:48:33',NULL,NULL),(188,'31-407-100-201','Electric Circuits','วงจรไฟฟ้า','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(189,'31-407-104-101','Electronic Devices for Computer Engineering','อุปกรณ์อิเล็กทรอนิกส์สำหรับวิศวกรรมคอมพิวเตอร์','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(190,'31-407-104-102','Electronic Devices for Computer Engineering Laboratory','ปฏิบัติการอุปกรณ์อิเล็กทรอนิกส์สำหรับวิศวกรรมคอมพิวเตอร์','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(191,'31-407-103-202','Data Commication','การสื่อสารข้อมูล','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(192,'31-407-104-202','Digital System Design','การออกแบบระบบดิจิทัล','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(193,'31-407-103-201','Discrete Mathematics for Engineering','คณิตศาสตร์ดิสครีตสำหรับวิศวกรรม','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(194,'31-407-100-104','Basic Computer Engineering Practice','การฝึกพื้นฐานทางวิศวกรรมคอมพิวเตอร์','3(1-6-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(195,'31-407-102-201','Object-Oriented Programming','การเขียนโปรแกรมเชิงวัตถุ','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(196,'00-000-041-002','Science and Modern Technology','วิทยาศาสตร์และเทคโนโลยีสมัยใหม่','3(3-0-6)','1.4','2022-10-04 00:48:33',NULL,NULL),(197,'03-407-101-210','Computer Architecture and Organization 2','สถาปัตยกรรมคอมพิวเตอร์และระบบ 2','2(2-0-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(198,'03-407-101-301','Digital System Design','การออกแบบระบบดิจิทัล','2(2-0-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(199,'03-407-101-302','Digital System Design Laboratory','ปฏิบัติการออกแบบระบบดิจิทัล','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(200,'03-407-101-303','Database System','ระบบฐานข้อมูล','2(2-0-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(201,'03-407-101-304','Database System Laboratory','ปฏิบัติการระบบฐานข้อมูล','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(202,'03-407-101-305','Computer Network','เครือข่ายคอมพิวเตอร์','2(2-0-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(203,'03-407-101-306','Computer Network Laboratory','ปฏิบัติการเครือข่ายคอมพิวเตอร์','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(204,'03-407-101-307','Microcontroller','ไมโครคอนโทรลเลอร์','2(2-0-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(205,'03-407-101-308','Microcontroller Laboratory','ปฏิบัติการไมโครคอนโทรลเลอร์','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(206,'03-407-101-402','Cooperative Education for Computer','สหกิจศึกษาสำหรับวิศวกรรมคอมพิวเตอร์','6(0-40-0)','2.2','2022-10-04 00:48:33',NULL,NULL),(207,'04-061-201','Statistics for Computer Engineering','สถิติสำหรับวิศวกรรมคอมพิวเตอร์','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(208,'04-061-401','Software Engineering ','วิศวกรรมซอฟต์แวร์','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(209,'04-061-404','Computer Engineering Project 2','โครงงานวิศวกรรมคอมพิวเตอร์ 2','3(1-6-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(210,'03-407-101-101','Electronic Devices for Computer Engineering','อุปกรณ์อิเล็กทรอนิกส์สำหรับวิศวกรรมคอมพิวเตอร์','2(2-0-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(211,'03-407-101-102','Electronic Devices for Computer Engineering Laboratory','ปฏิบัติการอุปกรณอิเล็กทรอนิกส์สำหรับวิศวกรรมคอมพิวเตอร์','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(212,'31-407-104-201','Digital and Logic Circuits','วงจรดิจิทัลและลอจิก','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(213,'31-407-103-301','Computer Network','เครือข่ายคอมพิวเตอร์','3(2-3-5)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(214,'03-407-101-202','Discrete Mathematics for Engineering','คณิตศาสตร์ดิสครีตสำหรับวิศวกรรม','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(215,'31-407-104-301','Computer Architecture and Organization','สถาปัตยกรรมและโครงสร้างคอมพิวเตอร์','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(216,'31-407-102-301','Databass System','ระบบฐานข้อมูล','3(2-3-5)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(217,'03-407-101-310','Computer Engineering Project 1','โครงงานวิศวกรรมคอมพิวเตอร์ 1','2(1-3-3)','2.2','2022-10-04 00:48:33',NULL,NULL),(218,'31-407-070-202','Thermodynamics','เทอร์โมไดนามิกส์','3(3-0-6)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(219,'31-407-070-203','Fluid Mechanics','กลศาสตร์ของไหล','3(3-0-6)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(220,'31-407-073-016','Engines Diagnosis','การวิเคราะห์เครื่องยนต์','3(2-3-5)','2.3.1','2022-10-04 00:48:33',NULL,NULL),(221,'31-407-032-201','Fundamentals of Electrical Engineering','หลักมูลของวิศวกรรมไฟฟ้า','3(2-3-5)','2.3.1','2022-10-04 00:48:33',NULL,NULL),(222,'31-407-073-020','Millwright Skills Workshop','การปฏิบัติงานของช่างเครื่องกลในโรงงาน','2(0-6-2)','2.3.1','2022-10-04 00:48:33',NULL,NULL),(223,'31-407-071-302','Computer Aided Design for Mechanical Engineering','คอมพิวเตอร์ช่วยในการออกแบบทางวิศวกรรมเครื่องกล','3(2-3-5)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(224,'31-407-071-303','Mechanics of Machinery','กลศาสตร์เครื่องจักรกล','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(225,'31-407-071-304','Machine Design','การออกแบบเครื่องจักรกล','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(226,'31-407-072-301','Refrigeration and Air Conditioning','การทำความเย็นและการปรับอากาศ','3(3-0-6)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(227,'31-407-072-302','Mechanical Engineering Laboratory 1','ปฏิบัติการวิศวกรรมเครื่องกล 1','2(0-6-2)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(228,'31-407-073-011','Industrial Pneumatics and Hydraulics','นิวแมติกส์และไฮดรอลิกส์อุตสาหกรรม','3(2-3-5)','2.3','2022-10-04 00:48:33',NULL,NULL),(229,'00-000-041-005','Entrepreneurship in Science and Technology','การเป็นผู้ประกอบการทางวิทยาศาสตร์และเทคโนโลยี','3(3-0-6)','1.4','2022-10-04 00:48:33',NULL,NULL),(230,'31-407-071-406','Automatic Control','การควบคุมอัตโนมัติ','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(231,'31-407-071-407','Mechanical Vibration','การสั่นสะเทือนทางกล','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(232,'31-407-071-408','Mechanical Engineering Project Seminar','สัมมนาโครงงานวิศวกรรมเครื่องกล','1(0-3-1)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(233,'31-407-072-403','Power Plant Engineering','วิศวกรรมโรงจักรต้นกำลัง','3(3-0-6)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(234,'31-407-072-404','Mechanical Engineering Laboratory 2','ปฏิบัติการวิศวกรรมเครื่องกล 2','2(0-6-2)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(235,'31-407-073-001','Internal Combustion Engines','เครื่องยนต์สันดาปภายใน','3(3-0-6)','2.3','2022-10-04 00:48:33',NULL,NULL),(236,'31-407-075-002','Railway Electrification System','ระบบการจ่ายไฟฟ้าแก่ทางรถไฟ','3(3-0-6)','2.3.2','2022-10-04 00:48:33',NULL,NULL),(237,'31-407-075-008','Fundamentals of Railway Vehicle Dynamics','พื้นฐานพลวัตรถราง','3(3-0-6)','2.3.2','2022-10-04 00:48:33',NULL,NULL),(238,'31-407-074-302','Railway Engineering Laboratory 1','ปฏิบัติการวิศวกรรมระบบราง 1','2(0-6-2)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(239,'31-407-075-006','Railway Braking System','ระบบเบรกรถไฟ','3(2-3-5)','2.3','2022-10-04 00:48:33',NULL,NULL),(240,'31-407-075-007','Air conditioning in Railway Vehicles','การปรับอากาศในรถราง','3(2-3-5)','2.3','2022-10-04 00:48:33',NULL,NULL),(241,'31-407-074-403','Friction and Wear','การเสียดทานและการสึกหรอ','3(3-0-6)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(242,'31-407-074-404','Railway Engineering Laboratory 2','ปฏิบัติการวิศวกรรมระบบราง 2','2(0-6-2)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(243,'31-407-075-009','Structure and design of locomotives','โครงสร้างและการออกแบบหัวรถจักร','3(3-0-6)','2.3','2022-10-04 00:48:33',NULL,NULL),(244,'00-000-031-204','English Conversation for Daily Life','สนทนาภาษาอังกฤษในชีวิตประจำวัน','3(3-0-6)','1.3','2022-10-04 00:48:33',NULL,NULL),(245,'31-407-050-103','Manufacturing Processes','กระบวนการผลิต','3(3-0-6)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(246,'31-407-070-205','Mechanics of Materials','กลศาสตร์วัสดุ','3(3-0-6)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(247,'31-407-071-201','Heat Transfer','การถ่ายโอนความร้อน','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(248,'31-407-071-305','Cooperative Education for Mechanical Engineering','สหกิจศึกษา สำหรับวิศวกรรมเครื่องกล','6(0-40-0)','2.2','2022-10-04 00:48:33',NULL,NULL),(249,'31-407-071-409','Mechanical Engineering Project','โครงงานวิศวกรรมเครื่องกล','3(1-6-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(250,'31-407-070-101','Basic Training in Mechanical Engineering','การฝึกพื้นฐานทางวิศวกรรมเครื่องกล','2(0-6-2)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(251,'31-407-070-102','Engineering Mechanics','กลศาสตร์วิศวกรรม','3(3-0-6)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(252,'31-407-082-304','Automatic Greenhouse Conrol System','ระบบควบคุมโรงเรือนปลูกพืชอัตโนมัติ','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(253,'31-407-080-303','Fundamentals of Mechanical Engineering','ปฏิบัติการวิศวกรรมเครื่องกลพื้นฐานสำหรับวิศวกรรมเครื่องจักรกลเกษตร','2(0-6-2)','2.2','2022-10-04 00:48:33',NULL,NULL),(254,'31-407-082-301','Pneumatics and Hydraulics for Agricultural Engineering','นิวเเมติกส์และไฮดรอลิกส์สำหรับวิศวกรรมเกษตร','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(255,'31-407-083-401','Agricultural Process Engineering','วิศวกรรมกระบวนการผลิตเกษตร','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(256,'31-407-083-303','Renewable Energy for Smart Agriculture ','พลังงานทดแทนเพื่อการเกษตรอัจฉริยะ','3(2-3-5)','3','2022-10-04 00:48:33',NULL,NULL),(257,'03-407-075-302','Refrigeration','การทำความเย็น','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(258,'03-407-071-308','Pre-Cooperative Education for Engineering','การเตรียมความพร้อมก่อนสหกิจศึกษาสำหรับวิศวกรรม','2(2-0-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(259,'03-407-071-408','Power Plant Engineering','วิศวกรรมโรงจักรต้นกำลัง','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(260,'03-407-071-409','Mechanical Vibration','การสั่นสะเทือนทางกล','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(261,'03-407-071-410','Automatic Control','การควบคุมอัตโนมัติ','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(262,'03-407-081-405','Agricultural Machinery Engineering Project','โครงงานวิศวกรรมเครื่องจักรกลเกษตร','3(1-6-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(263,'03-407-082-411','Repair and Maintenance Engineering in Agricultural Industry','วิศวกรรมการซ่อมบำรุงในอุตสาหกรรมเกษตร','2(1-3-3)','3','2022-10-04 00:48:33',NULL,NULL),(264,'03-407-082-308','Agricultural Power','เครื่องต้นกำลังทางการเกษตร','2(1-3-3)','3','2022-10-04 00:48:33',NULL,NULL),(265,'03-407-083-404','Material Handling Engineering','วิศวกรรมขนถ่ายวัสดุ','2(1-3-3)','3','2022-10-04 00:48:33',NULL,NULL),(266,'04-030-204','Engineering Dynamics','พลศาสตร์วิศวกรรม','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(267,'31-407-084-403','Cooperative Education for Agricultural Machinery','สหกิจศึกษาสำหรับวิศวกรรมเครื่องจักรกลเกษตร','6(0-40-0)','2.2','2022-10-04 00:48:33',NULL,NULL),(268,'31-407-082-306','Smart Agricultural Tractor','รถแทรกเตอร์เกษตรอัจฉริยะ','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(269,'31-407-460-101','Applied Mathematics','คณิตศาสตร์ประยุกต์','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(270,'31-407-460-102','Engineering Drawing','เขียนแบบวิศวกรรม','2(2-0-4)','2.1','2022-10-04 00:48:33',NULL,NULL),(271,'31-407-460-103','Practice on Engineering Drawing','การฝึกปฏิบัติเขียนแบบวิศวกรรม','1(0-3-1)','2.1','2022-10-04 00:48:33',NULL,NULL),(272,'31-407-460-104','Engineering Mechanics','กลศาสตร์วิศวกรรม','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(273,'31-407-460-105','Computer Programming','การเขียนโปรแกรมคอมพิวเตอร์','2(2-0-4)','2.1','2022-10-04 00:48:33',NULL,NULL),(274,'31-407-460-106','Practice on Computer Programming','การฝึกปฏิบัติการเขียนโปรแกรมคอมพิวเตอร์','1(0-3-1)','2.1','2022-10-04 00:48:33',NULL,NULL),(275,'31-407-462-208','Supply Chain Management and Logistics in Sugarcane and Sugar Industry','การจัดการโซ่อุปทานและโลจิสติกส์ในอุตสาหกรรมอ้อยและน้้าตาล','3(3-0-6)','3','2022-10-04 00:48:33',NULL,NULL),(276,'31-407-461-207','Sugarcane and Sugar Production Process','กระบวนการผลิตอ้อยและน้ำตาล','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(277,'31-407-463-201','Pneumatics and Hydraulics in Sugarcane and Sugar Industry','นิวเมติกส์และไฮดรอลิกส์ในอุตสาหกรรมอ้อยและน้ำตาล','2(2-0-4)','2.4','2022-10-04 00:48:33',NULL,NULL),(278,'31-407-463-202','Practice on Pneumatics and Hydraulics in Sugarcane and Sugar Industry','การฝึกปฏิบัตินิวเมติกส์และไฮดรอลิกส์ในอุตสาหกรรมอ้อยและน้ำตาล','1(0-3-1)','2.4','2022-10-04 00:48:33',NULL,NULL),(279,'31-407-463-203','Tractor Engineering for Modern Farm','วิศวกรรมแทรกเตอร์เพื่อการเกษตรสมัยใหม่','2(2-0-4)','2.4','2022-10-04 00:48:33',NULL,NULL),(280,'31-407-463-204','Practice on Tractor Engineering for Modern Farm','การฝึกปฏิบัติวิศวกรรมแทรกเตอร์เพื่อการเกษตรสมัยใหม่','2(0-6-2)','2.4','2022-10-04 00:48:33',NULL,NULL),(281,'31-407-463-205','Agricultural Machinery Engineering','วิศวกรรมเครื่องจักรกลเกษตร','2(2-0-4)','2.4','2022-10-04 00:48:33',NULL,NULL),(282,'31-407-463-206','Practice on Agricultural Machinery Engineering','การฝึกปฏิบัติวิศวกรรมเครื่องจักรกลเกษตร','2(0-6-2)','2.4','2022-10-04 00:48:33',NULL,NULL),(283,'31-407-463-214','Pre-Project on Modern Technology of Sugarcane and Sugar Industry','เตรียมโครงงานเทคโนโลยีสมัยใหม่ทางอุตสาหกรรมอ้อยและน้ำตาล','1(1-0-2)','2.4','2022-10-04 00:48:33',NULL,NULL),(284,'31-407-462-203','Unmanned Aerial Vehicle and Image Processing in Sugarcane and Sugar Industry','อากาศยานไร้คนขับและการประมวลผลภาพในอุตสาหกรรมอ้อยและน้ำตาล','2(2-0-4)','2.3','2022-10-04 00:48:33',NULL,NULL),(285,'31-407-462-204','Practice on Unmanned Aerial Vehicle and Image Processing in Sugarcane and Sugar Industry','การฝึกปฏิบัติอากาศยานไร้คนขับและการประมวลผลภาพในอุตสาหกรรมอ้อยและน้ำตาล','1(0-3-1)','2.3','2022-10-04 00:48:33',NULL,NULL),(286,'31-407-463-209','Maintenance of Agricultural Machinery in Sugarcane and Sugar Industry','การบำรุงรักษาเครื่องจักรกลเกษตรในอุตสาหกรรมอ้อยและน้ำตาล','2(2-0-4)','2.4','2022-10-04 00:48:33',NULL,NULL),(287,'31-407-463-210','Practice on Maintenance of Agricultural Machinery in Sugarcane and Sugar Industry','การฝึกปฏิบัติการบำรุงรักษาเครื่องจักรกลเกษตรในอุตสาหกรรมอ้อยและน้ำตาล','2(0-6-2)','2.4','2022-10-04 00:48:33',NULL,NULL),(288,'31-407-420-201','Modern Technology in Food Industry','เทคโนโลยีสมัยใหม่ในอุตสาหกรรมอาหาร','3(2-3-5)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(289,'03-407-071-302','Machine Design','การออกแบบเครื่องจักรกล','4(4-0-8)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(290,'31-407-035-310','Heat and Mass Transfer','การถ่ายโอนความร้อนและการแพร่ของมวล','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(291,'03-407-091-202','Fundamentals Biochemistry and Microbiology for Agriculture and Food Industry','ชีวเคมีและจุลชีววิทยาเบื้องต้นสำหรับอุตสาหกรรมเกษตรและอาหาร','2(2-0-4)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(292,'03-407-091-201','Post-Harvest and Processing Engineering Laboratory','ปฏิบัติการด้านวิศวกรรมหลังการเก็บเกี่ยวและแปรสภาพ','1(0-3-1)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(293,'03-407-091-204','Post-Harvest Engineering and Technology of Agricultural Products','วิศวกรรมและเทคโนโลยีหลังการเก็บเกี่ยวผลิตผลเกษตร','3(2-3-5)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(294,'03-407-091-305','Quality Control of Agriculture and Food Industry','การควบคุมคุณภาพอุตสาหกรรมเกษตรและอาหาร','3(3-0-6)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(295,'31-407-092-301','Computer Aided Engineering for Post-Harvest and Processing Engineering','คอมพิวเตอร์ช่วยงานวิศวกรรมสำหรับวิศวกรรมหลังการเก็บเกี่ยวและแปรสภาพ','3(2-3-5)','2.3.2','2022-10-04 00:48:33',NULL,NULL),(296,'03-407-092-401','Cooperative Education for Post-Harvest and Processing Engineering','สหกิจศึกษาสำหรับวิศวกรรมหลังการเก็บเกี่ยวและแปรสภาพ','6(0-40-0)','2.3.1','2022-10-04 00:48:33',NULL,NULL),(297,'04-091-402','Engineering and Processing Engineering Projects 2','โครงงานด้านวิศวกรรมหลังการเก็บเกี่ยวและแปรสภาพ 2','3(1-6-4)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(298,'04-091-307','Agricultural and Food Equipment','เครื่องมือแปรสภาพผลิตผลเกษตรและอาหาร','3(2-3-5)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(299,'04-091-302','Power Systems in Food Industry','ระบบต้นกำลังในอุตสาหกรรมอาหาร','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(300,'04-091-301','Combustion Technology for Food Engineering','เทคโนโลยีการเผาไหม้สำหรับวิศวกรรมอาหาร','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(301,'31-407-030-221','Basic Electrical Engineering','วิศวกรรมไฟฟ้าพื้นฐาน','3(3-0-6)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(302,'31-407-030-222','Basic Electrical Engineering Laboratory','ปฏิบัติการวิศวกรรมไฟฟ้าพื้นฐาน','1(0-3-1)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(303,'31-407-051-203','Metrology Engineering Laboratory','ปฏิบัติการวิศวกรรมมาตรวิทยา','2(1-3-3)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(304,'02-005-011-110','Calculus 2 for Engineers','แคลคูลัส 2 สำหรับวิศวกร','3(3-0-6)','2.1.1','2022-10-04 00:48:33',NULL,NULL),(305,'31-407-050-204','Engineering Statistics','สถิติวิศวกรรม','3(3-0-6)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(306,'31-407-051-202','Welding and Sheet Metal Practice','ปฏิบัติงานเชื่อมและโลหะแผ่น','3(1-6-4)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(307,'31-407-051-308','Production Planning and Control','การวางแผนและควบคุมการผลิต','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(308,'31-407-051-307','Quality Control','การควบคุมคุณภาพ','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(309,'31-407-051-309','Industrial Plant Design','การออกแบบโรงงานอุตสาหกรรม','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(310,'31-407-052-202','Maintenance Engineering','วิศวกรรมการบำรุงรักษา','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(311,'31-407-052-305','Industrial Engineering Pre-Project','การเตรียมโครงงานวิศวกรรมอุตสาหการ','1(1-0-2)','2.2','2022-10-04 00:48:33',NULL,NULL),(312,'31-407-054-007','Design of Industrial Engineering Experiments','การออกแบบการทดลองทางวิศวกรรมอุตสาหการ','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(313,'31-407-056-004','Automatic Machine ','เครื่องจักรกลอัตโนมัติ','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(314,'31-407-053-305','Production Engineering Pre-Project','การเตรียมโครงงานวิศวกรรมการผลิต','1(1-0-2)','2.2','2022-10-04 00:48:33',NULL,NULL),(315,'31-407-056-010','Design of Production Engineering Experiments','การออกแบบการทดลองทางวิศวกรรมการผลิต','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(316,'31-407-053-202','Machine Tool','งานเครื่องมือกล','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(317,'31-407-050-305','Industrial Engineering Laboratory','ปฏิบัติการวิศวกรรมอุตสาหการ','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(318,'31-407-051-310','Engineering Economy','เศรษฐศาสตร์วิศวกรรม','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(319,'31-407-053-303','Automatic and Control System','ระบบควบคุมอัตโนมัติ','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(320,'31-407-053-304','Forming Process','กระบวนการขึ้นรูปวัสดุ','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(321,'04-041-304','Operations Research','การวิจัยการดำเนินงาน','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(322,'00-000-041-001','Life and Environment','ชีวิตและสิ่งแวดล้อม','3(3-0-6)','1.4','2022-10-04 00:48:33',NULL,NULL),(323,'31-407-051-204','Safety Engineering','วิศวกรรมความปลอดภัย','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(324,'31-407-051-206','Industrial Work Study','การศึกษางานอุตสาหกรรม','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(325,'31-407-073-019','Basic Mechanical Engineering Laboratory','ปฏิบัติการวิศวกรรมเครื่องกลพื้นฐาน','1(0-3-1)','2.1.2','2022-10-04 00:48:33',NULL,NULL),(326,'31-407-053-201','Tool Engineering','วิศวกรรมเครื่องมือ','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(327,'31-407-056-002','Computer Aided Design and Manufacturing','คอมพิวเตอร์ช่วยในการออกแบบและการผลิต','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(328,'31-407-053-407','Cooperative Education for Industrial Engineering','สหกิจศึกษาสำหรับวิศวกรรมการผลิต','6(0-40-0)','2.2','2022-10-04 00:48:33',NULL,NULL),(329,'31-407-052-201','Operations Research','การวิจัยการดำเนินงาน','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(330,'31-407-053-406','Production Engineering Project','โครงงานวิศวกรรมการผลิต','3(1-6-4)','2.2.2','2022-10-04 00:48:33',NULL,NULL),(331,'31-407-387-309','Fundamental Chemistry of Dairy and Beverage','เคมีพื้นฐานผลิตภัณฑ์นมและเครื่องดื่ม','2(0-4-2)','2.2.3','2022-10-04 00:48:33',NULL,NULL),(332,'31-407-387-316','Practice on Fundamental Processing of Dairy and Beverage','การฝึกปฏิบัติกระบวนการแปรรูปพื้นฐานผลิตภัณฑ์นมและเครื่องดื่ม','1(0-2-1)','2.2.3','2022-10-04 00:48:33',NULL,NULL),(333,'31-407-387-311','Fundamental Microbiology of Dairy and Beverage','จุลชีววิทยาพื้นฐานผลิตภัณฑ์นมและเครื่องดื่ม','2(2-0-4)','2.2.3','2022-10-04 00:48:33',NULL,NULL),(334,'31-407-387-312','Practice on Fundamental Microbiology of Dairy and Beverage','การฝึกปฏิบัติจุลชีววิทยาพื้นฐานผลิตภัณฑ์นมและเครื่องดื่ม','2(0-4-2)','2.2.3','2022-10-04 00:48:33',NULL,NULL),(335,'31-407-387-313','Equipment and Unit Operation for Dairy and Beverage Processing','เครื่องมือและหน่วยปฏิบัติการสำหรับกระบวนการแปรรูปผลิตภัณฑ์นมและเครื่องดื่ม','2(2-0-4)','2.2.3','2022-10-04 00:48:33',NULL,NULL),(336,'31-407-387-314','Practice on Equipment and Unit Operation for Dairy and Beverage Processing','การฝึกปฏิบัติเครื่องมือและหน่วยปฏิบัติการสำหรับกระบวนการแปรรูปผลิตภัณฑ์นมและเครื่องดื่ม','2(0-4-2)','2.2.3','2022-10-04 00:48:33',NULL,NULL),(337,'31-407-387-317','Utility Engineering for Dairy and Beverages Processing','วิศวกรรมสาธารณูปโภคสำหรับกระบวนการแปรรูปผลิตภัณฑ์นมและเครื่องดื่ม','3(3-0-6)','2.2.3','2022-10-04 00:48:33',NULL,NULL),(338,'31-407-387-424','Dairy and Beverage Engineering Pre-Project','การเตรียมโครงงานวิศวกรรมผลิตภัณฑ์นมและเครื่องดื่ม','1(1-0-2)','2.2.3','2022-10-04 00:48:33',NULL,NULL),(339,'00-000-021-002','Knowledge Management','การจัดการความรู้','3(3-0-6)','1.2','2022-10-04 00:48:33',NULL,NULL),(340,'02-405-011-109','Calculus 1 for Engineers','แคลคูลัส 1 สำหรับวิศวกร','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(341,'02-405-020-105','Fundamentals of Chemistry','เคมีพื้นฐาน','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(342,'02-405-020-106','Fundamentals of Chemistry Laboratory','ปฏิบัติการเคมีพื้นฐาน','1(0-3-1)','2.1','2022-10-04 00:48:33',NULL,NULL),(343,'02-405-011-110','Calculus 2 for Engineers','แคลคูลัส 2 สำหรับวิศวกร','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(344,'31-407-051-101','Machine Tool Practice','ปฏิบัติงานเครื่องมือกล','3(1-6-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(345,'31-407-121-102','Metallurgy of Metals Joining','โลหะวิทยาการเชื่อมต่อโลหะ','3(2-3-5)','2.2','2022-10-04 00:48:33',NULL,NULL),(346,'31-407-121-202','Ferrous and Non Ferrous Metals','โลหะกลุ่มเหล็กและโลหะนอกกลุ่มเหล็ก','2(1-3-3)','2.2','2022-10-04 00:48:33',NULL,NULL),(347,'31-407-121-203','Foundry Engineering 1','วิศวกรรมหล่อโลหะ 1','3(1-6-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(348,'31-407-030-203','Electrical Engineering Technology','เทคโนโลยีวิศวกรรมไฟฟ้า','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(349,'31-407-050-104','Engineering Statistics','31-407-050-104','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(350,'31-407-121-101','Metal Forming','การขึ้นรูปโลหะ','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(351,'31-407-121-301','Thermal Treatment of Metals','การปรับปรุงสมบัติโลหะด้วยความร้อน','2(1-3-3)','2.2','2022-10-04 00:48:33',NULL,NULL),(352,'31-407-123-201','Welding Engineering 1','วิศวกรรมเชื่อมโลหะ 1','3(2-3-5)','2.3','2022-10-04 00:48:33',NULL,NULL),(353,'31-407-122-301','Foundry Engineering 2','วิศวกรรมหล่อโลหะ 2','3(1-6-4)','2.3','2022-10-04 00:48:33',NULL,NULL),(354,'31-407-122-302','Pattern Design and Marking in Casting','การออกแบบและสร้างกระสวนในงานหล่อ','2(1-3-3)','2.3','2022-10-04 00:48:33',NULL,NULL),(355,'31-407-121-401','Cooperative Education for Metallurgical','สหกิจศึกษาสำหรับวิศวกรรมโลหการ','6(0-40-0)','2.2','2022-10-04 00:48:33',NULL,NULL),(356,'00-000-034-001','Chinese Conversation for Daily Life','การสนทนาภาษาจีนในชีวิตประจำวัน','3(3-0-6)','1.3','2022-10-04 00:48:33',NULL,NULL),(357,'02-005-011-104','Calculus 1','แคลคูลัส 1','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(358,'02-005-020-101','General Chemistry 1','เคมีทั่วไป 1','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(359,'02-005-020-102','General Chemistry Laboratory 1','ปฏิบัติการเคมีทั่วไป 1','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(360,'02-005-040-102','Biology','ชีววิทยา','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(361,'02-005-040-103','Biology Laboratory','ปฏิบัติการชีววิทยา','1(0-3-1)','2.1','2022-10-04 00:48:33',NULL,NULL),(362,'02-005-050-204','Statistic 1','สถิติ 1','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(363,'02-405-021-301','Inorganic Chemistry 1','เคมีอนินทรีย์ 1','3(3-0-6)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(364,'02-405-021-302','Inorganic Chemistry Laboratory 1','ปฏิบัติการเคมีอนินทรีย์ 1','1(0-3-1)','2.2.1','2022-10-04 00:48:33',NULL,NULL),(365,'31-405-027-201','Chemicals and Safety',NULL,NULL,'2.2.1','2022-10-04 00:48:33',NULL,'2022-10-04 15:57:09'),(366,'02-405-023-301','Biochemistry','ชีวเคมี','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(367,'02-405-023-302','Biochemistry Laboratory','ปฏิบัติการชีวเคมี','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(368,'02-405-025-305','Separation Techniques','เทคนิคการแยกสาร','3(3-0-6)','2.2','2022-10-04 00:48:33',NULL,NULL),(369,'02-405-025-306','Separation Techniques Laboratory','ปฏิบัติการเทคนิคการแยกสาร','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(370,'02-405-027-302','Chemical and Safety','สารเคมีและความปลอดภัย','2(2-0-4)','2.2','2022-10-04 00:48:33',NULL,NULL),(371,'02-405-027-305','Catalytic Chemistry','เคมีคะตะลิสต์','3(3-0-6)','2.3','2022-10-04 00:48:33',NULL,NULL),(372,'02-405-029-403','Pre-Cooperative Education','เตรียมสหกิจศึกษา','2(2-0-4)','2.3','2022-10-04 00:48:33',NULL,NULL),(373,'02-405-026-301','Chemical Process Industries','กระบวนการอุตสาหกรรมเคมี','3(3-0-6)','2.3','2022-10-04 00:48:33',NULL,NULL),(374,'02-405-028-301','Seminar','สัมมนา','1(0-3-1)','2.2','2022-10-04 00:48:33',NULL,NULL),(375,'02-405-026-304','Polymer Technology','เทคโนโลยีพอลิเมอร์','3(3-0-6)','3','2022-10-04 00:48:33',NULL,NULL),(376,'02-405-021-310','Selected Topics in Chemistry','หัวข้อที่น่าสนใจทางเคมี','3(3-0-6)','3','2022-10-04 00:48:33',NULL,NULL),(377,'31-405-160-101','General Physics 1','ฟิสิกส์ทั่วไป 1','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(378,'31-405-160-102','General Physics Laboratory 1','ปฏิบัติการฟิสิกส์ทั่วไป 1','1(0-3-1)','2.1','2022-10-04 00:48:33',NULL,NULL),(379,'31-405-160-103','Fundamentals of Mathematics 1','คณิตศาสตร์พื้นฐาน 1','3(3-0-6)','2.1','2022-10-04 00:48:33',NULL,NULL),(380,'12-345-678-901','Customer Behavier','พฤติกรรมผู้บริโภค','3(0-0-0)','2','2022-10-04 00:48:33',NULL,NULL),(381,'31-679-022-065','good subject','วิชาดีดี','3(3-0-6)','1.2','2022-10-04 20:34:22',NULL,NULL),(382,'00-755-643','dsads','sdd','3(3-0-6)','1.2','2022-10-04 23:01:03','2022-10-04 23:44:46','2022-10-04 23:46:59'),(383,'00-000-011-001','Social Dynamics and Happy Living','พลวัตทางสังคมกับการดำรงชีวิตอย่างมีความสุข','3(3-0-6)','1.2','2022-10-11 14:10:22',NULL,NULL);
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf3`
--

DROP TABLE IF EXISTS `tqf3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf3` (
  `tqf3ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Year_idYear` int(10) unsigned NOT NULL,
  `subject_idSubject` int(10) unsigned NOT NULL,
  `group_sub` int(5) DEFAULT NULL,
  `filetqf3` text DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `status` int(11) DEFAULT 0 COMMENT '1 = รอตรวจสอบ\n2 = ถูกต้อง\n3 = ส่งกลับ\n4 =',
  `send_file` int(11) DEFAULT 0,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `is_file` int(11) DEFAULT 0,
  `id_tqf5` int(11) DEFAULT NULL,
  `teacher` int(11) DEFAULT 0,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tqf3ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf3`
--

LOCK TABLES `tqf3` WRITE;
/*!40000 ALTER TABLE `tqf3` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf3_1`
--

DROP TABLE IF EXISTS `tqf3_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf3_1` (
  `tqf3_tqf3ID` int(10) unsigned NOT NULL,
  `THname` text NOT NULL,
  `ENname` text NOT NULL,
  `credit` text NOT NULL,
  `course` text NOT NULL,
  `teacher` text NOT NULL,
  `term` text NOT NULL,
  `Pre_requisite` text NOT NULL,
  `Co_requisite` text NOT NULL,
  `place` text NOT NULL,
  `date_modify` text NOT NULL,
  `status` varchar(5) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf3_tqf3ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf3_1`
--

LOCK TABLES `tqf3_1` WRITE;
/*!40000 ALTER TABLE `tqf3_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf3_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf3_2`
--

DROP TABLE IF EXISTS `tqf3_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf3_2` (
  `tqf3_tqf3ID` int(10) unsigned NOT NULL,
  `target` text NOT NULL,
  `objective` text NOT NULL,
  `status` varchar(5) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf3_tqf3ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf3_2`
--

LOCK TABLES `tqf3_2` WRITE;
/*!40000 ALTER TABLE `tqf3_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf3_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf3_3`
--

DROP TABLE IF EXISTS `tqf3_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf3_3` (
  `tqf3_tqf3ID` int(10) unsigned NOT NULL,
  `course_desc` text NOT NULL,
  `hour_lecture` text NOT NULL,
  `houre_practice` text NOT NULL,
  `hour_selhflearn` text NOT NULL,
  `hour_tu` text NOT NULL,
  `hour_recom` text NOT NULL,
  `status` varchar(5) NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf3_tqf3ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf3_3`
--

LOCK TABLES `tqf3_3` WRITE;
/*!40000 ALTER TABLE `tqf3_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf3_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf3_4`
--

DROP TABLE IF EXISTS `tqf3_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf3_4` (
  `tqf3_tqf3ID` int(10) unsigned NOT NULL,
  `morality` text NOT NULL,
  `morality2` text NOT NULL,
  `morality3` text NOT NULL,
  `knowledge` text NOT NULL,
  `knowledge2` text NOT NULL,
  `knowledge3` text NOT NULL,
  `intel_skill` text NOT NULL,
  `intel_skill2` text NOT NULL,
  `intel_skill3` text NOT NULL,
  `respon_skill` text NOT NULL,
  `respon_skill2` text NOT NULL,
  `respon_skill3` text NOT NULL,
  `num_skill` text NOT NULL,
  `num_skill2` text NOT NULL,
  `num_skill3` text NOT NULL,
  `status` varchar(5) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf3_tqf3ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf3_4`
--

LOCK TABLES `tqf3_4` WRITE;
/*!40000 ALTER TABLE `tqf3_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf3_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf3_5`
--

DROP TABLE IF EXISTS `tqf3_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf3_5` (
  `tqf3_tqf3ID` int(10) unsigned NOT NULL,
  `data1` text DEFAULT NULL,
  `data2` text DEFAULT NULL,
  `status` varchar(5) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf3_tqf3ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf3_5`
--

LOCK TABLES `tqf3_5` WRITE;
/*!40000 ALTER TABLE `tqf3_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf3_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf3_6`
--

DROP TABLE IF EXISTS `tqf3_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf3_6` (
  `tqf3_tqf3ID` int(10) unsigned NOT NULL,
  `detail1` text NOT NULL,
  `detail2` text NOT NULL,
  `detail3` text NOT NULL,
  `status` varchar(5) NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf3_tqf3ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf3_6`
--

LOCK TABLES `tqf3_6` WRITE;
/*!40000 ALTER TABLE `tqf3_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf3_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf3_7`
--

DROP TABLE IF EXISTS `tqf3_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf3_7` (
  `tqf3_tqf3ID` int(10) unsigned NOT NULL,
  `detail1` text NOT NULL,
  `detail2` text NOT NULL,
  `detail3` text NOT NULL,
  `detail4` text NOT NULL,
  `detail5` text NOT NULL,
  `status` varchar(5) NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf3_tqf3ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf3_7`
--

LOCK TABLES `tqf3_7` WRITE;
/*!40000 ALTER TABLE `tqf3_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf3_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf5`
--

DROP TABLE IF EXISTS `tqf5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf5` (
  `tqf5ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Year_idYear` int(10) unsigned NOT NULL,
  `subject_idSubject` int(10) unsigned NOT NULL,
  `group_sub` int(5) DEFAULT NULL,
  `filetqf5` text DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `status` int(11) DEFAULT 0 COMMENT '1 = รอตรวจสอบ\n2 = ถูกต้อง\n3 = ส่งกลับแก้ไข\n4 =',
  `send_file` int(11) DEFAULT 0,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `is_file` int(11) DEFAULT 0,
  `id_tqf3` int(11) DEFAULT NULL,
  `teacher` int(11) DEFAULT 0,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tqf5ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf5`
--

LOCK TABLES `tqf5` WRITE;
/*!40000 ALTER TABLE `tqf5` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf5_1`
--

DROP TABLE IF EXISTS `tqf5_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf5_1` (
  `tqf5_tqf5ID` int(10) unsigned NOT NULL,
  `THname` text NOT NULL DEFAULT '',
  `ENname` text NOT NULL DEFAULT '',
  `pre_req` text NOT NULL DEFAULT '',
  `teacher` text NOT NULL DEFAULT '',
  `term` text NOT NULL DEFAULT '',
  `place` text NOT NULL DEFAULT '',
  `status` varchar(5) NOT NULL DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf5_tqf5ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf5_1`
--

LOCK TABLES `tqf5_1` WRITE;
/*!40000 ALTER TABLE `tqf5_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf5_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf5_2`
--

DROP TABLE IF EXISTS `tqf5_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf5_2` (
  `tqf5_tqf5ID` int(10) unsigned NOT NULL,
  `data1_1` text DEFAULT '',
  `data1_2` text DEFAULT '',
  `data2_1` text NOT NULL DEFAULT '',
  `data2_2` text NOT NULL DEFAULT '',
  `data2_3` text NOT NULL DEFAULT '',
  `data4` text NOT NULL DEFAULT '',
  `status` varchar(5) NOT NULL DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf5_tqf5ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf5_2`
--

LOCK TABLES `tqf5_2` WRITE;
/*!40000 ALTER TABLE `tqf5_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf5_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf5_3`
--

DROP TABLE IF EXISTS `tqf5_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf5_3` (
  `tqf5_tqf5ID` int(10) unsigned NOT NULL,
  `num_regis` int(11) NOT NULL DEFAULT 0,
  `num_end` int(11) NOT NULL DEFAULT 0,
  `num_w` int(11) NOT NULL DEFAULT 0,
  `detail5` text NOT NULL DEFAULT '',
  `detail6_1` text NOT NULL DEFAULT '',
  `detail6_2` text NOT NULL DEFAULT '',
  `detail7` text NOT NULL DEFAULT '',
  `grade` text DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 0,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf5_tqf5ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf5_3`
--

LOCK TABLES `tqf5_3` WRITE;
/*!40000 ALTER TABLE `tqf5_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf5_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf5_4`
--

DROP TABLE IF EXISTS `tqf5_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf5_4` (
  `tqf5_tqf5ID` int(10) unsigned NOT NULL,
  `detail1_1` text NOT NULL DEFAULT '',
  `detail1_2` text NOT NULL DEFAULT '',
  `detail2_1` text NOT NULL DEFAULT '',
  `detail2_2` text NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 0,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf5_tqf5ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf5_4`
--

LOCK TABLES `tqf5_4` WRITE;
/*!40000 ALTER TABLE `tqf5_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf5_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf5_5`
--

DROP TABLE IF EXISTS `tqf5_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf5_5` (
  `tqf5_tqf5ID` int(10) unsigned NOT NULL,
  `detail1_1` text NOT NULL,
  `detail1_2` text NOT NULL,
  `detail2_1` text NOT NULL,
  `detail2_2` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf5_tqf5ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf5_5`
--

LOCK TABLES `tqf5_5` WRITE;
/*!40000 ALTER TABLE `tqf5_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf5_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tqf5_6`
--

DROP TABLE IF EXISTS `tqf5_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tqf5_6` (
  `tqf5_tqf5ID` int(10) unsigned NOT NULL,
  `detail1_1` text NOT NULL,
  `detail1_2` text NOT NULL,
  `detail2` text NOT NULL,
  `detail3_1` text NOT NULL,
  `detail3_2` text NOT NULL,
  `detail3_3` text NOT NULL,
  `detail4` text NOT NULL,
  `status` varchar(5) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`tqf5_tqf5ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tqf5_6`
--

LOCK TABLES `tqf5_6` WRITE;
/*!40000 ALTER TABLE `tqf5_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `tqf5_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_tqf3`
--

DROP TABLE IF EXISTS `user_tqf3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tqf3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tqf3ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tqf3`
--

LOCK TABLES `user_tqf3` WRITE;
/*!40000 ALTER TABLE `user_tqf3` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_tqf3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_tqf5`
--

DROP TABLE IF EXISTS `user_tqf5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tqf5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tqf5ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tqf5`
--

LOCK TABLES `user_tqf5` WRITE;
/*!40000 ALTER TABLE `user_tqf5` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_tqf5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level_LevelID` int(10) unsigned NOT NULL,
  `branch_idBranch` int(10) unsigned NOT NULL,
  `Uprefix` varchar(10) NOT NULL,
  `UFName` varchar(255) NOT NULL,
  `ULName` varchar(255) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `userCode` varchar(10) DEFAULT NULL,
  `chairman` int(11) DEFAULT 0,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `is_editor` int(11) DEFAULT 1,
  PRIMARY KEY (`userID`),
  KEY `users_level_levelid_foreign` (`level_LevelID`),
  CONSTRAINT `users_level_levelid_foreign` FOREIGN KEY (`level_LevelID`) REFERENCES `level` (`levelID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,6,'นางสาว','ชื่อเจ้าหน้าที่01','สกุลเจ้าหน้าที่01','officer.01',NULL,NULL,NULL,0,'2022-10-05 15:32:34',NULL,NULL,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `year`
--

DROP TABLE IF EXISTS `year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `year` (
  `idYear` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Year` varchar(100) NOT NULL,
  `term` varchar(100) NOT NULL,
  `active` int(5) DEFAULT 0,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idYear`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `year`
--

LOCK TABLES `year` WRITE;
/*!40000 ALTER TABLE `year` DISABLE KEYS */;
INSERT INTO `year` VALUES (1,'2565','1',0,'2022-11-06 18:59:04',NULL,NULL),(2,'2565','2',0,'2022-11-06 18:59:24',NULL,NULL);
/*!40000 ALTER TABLE `year` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tesss'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-06 19:03:35
