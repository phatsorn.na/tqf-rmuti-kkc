<h3>config server</h3>

```
sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt install php7.3-fpm
sudo apt install openssl php7.3-common php7.3-curl php7.3-json php7.3-mbstring php7.3-mysql php7.3-xml php7.3-zip php7.3-gd php7.3-zip

-------------------------------------------
หาก มี php8 อยู่ใน server
sudo apt remove php8.0
sudo apt-get autoremove --purge php-fpm
sudo apt-get autoremove --purge php8.0-fpm
sudo apt-get purge php8.*
--------------------------------------------

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
sudo apt install nginx
sudo apt install git

git clone https://gitlab.com/phatsorn.na/tqf-rmuti-kkc.git
sudo cp -r tqf-rmuti-kkc /var/www/html/

cd /var/www/html/tqf-rmuti-kkc
composer update

sudo chown www-data:www-data /var/www/html/tqf-rmuti-kkc/

sudo cp .env .env.example
sudo php artisan key:generate
sudo nano .env

====-------------------------------------------------------------=====
แก้ไขไฟล์ .env
APP_URL=http://203.158.201.67

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=tqf
DB_USERNAME=tqf
DB_PASSWORD=tqf1234

SSO_Login="https://service.eng.rmuti.ac.th/sso/gw/login/?id=tqfnmp&&msg=Login Seccess" 
SSO_Logout="https://service.eng.rmuti.ac.th/sso/gw/logout?id=tqfnmp&&msg=Logout Success"

====-----------------------------------------------------------------=====

sudo chown -R www-data:www-data /var/www/html/tqf-rmuti-kkc/storage
sudo chown -R www-data:www-data /var/www/html/tqf-rmuti-kkc/bootstrap/cache


sudo php artisan config:cache
sudo php artisan cache:clear
sudo php artisan config:clear
cd


cd /etc/nginx
sudo cat sites-enabled/default
sudo nano sites-enabled/default

```

**แก้ไข sites-enabled/default**
```
server {
    listen 8080;
    server_name 203.158.201.67;
    index index.php index.html;
    root /var/www/html/tqf-rmuti-kkc/public;
location ~* \.(jpg|jpeg|gif|css|png|js|ico|html)$ {
access_log off;
expires max;
log_not_found off;
}

# removes trailing slashes (prevents SEO duplicate content issues)
if (!-d $request_filename)
{
rewrite ^/(.+)/$ /$1 permanent;
}

# enforce NO www
if ($host ~* ^www\.(.*))
{
set $host_without_www $1;
rewrite ^/(.*)$ $scheme://$host_without_www/$1 permanent;
}

# unless the request is for a valid file (image, js, css, etc.), send to bootstrap
if (!-e $request_filename)
{
rewrite ^/(.*)$ /index.php?/$1 last;
break;
}

location / {
    try_files $uri $uri/ /index.php?$args;
}

location ~ \.php$ {
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
    fastcgi_index index.php;
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_param PATH_INFO $fastcgi_path_info;

}
}
```

```
sudo nginx -t
sudo systemctl reload nginx
sudo systemctl restart nginx.service
```


**ฐานข้อมูล**
```
sudo apt install mariadb-server-10.3
sudo mysql
CREATE USER 'tqf'@'localhost' IDENTIFIED BY 'tqf1234';
GRANT ALL PRIVILEGES ON *.* TO 'tqf'@'localhost';
FLUSH PRIVILEGES;
exit

sudo mysql -u tqf -p
CREATE DATABASE tqf;
```



